*************************************
:mod:`pybpodapi`--- pybpod-api module
*************************************

.. module:: pybpodapi
   :synopsis: top-level module

.. toctree::
   :titlesonly:

   bpod/index
   bpod_modules/index
   com/index
   exceptions/index
   state_machine/index
   session
   
Communication module
---------------------

.. toctree::
   :titlesonly:

   com/index
