********
Diagrams
********

=============================
Class Diagrams (and modules)
=============================

Main entities
-------------

.. image:: /_images/pybpodapi_main_classes.svg
   :scale: 100 %

Inheritance
-----------

.. image:: /_images/pybpodapi_inheritance.svg
   :scale: 100 %

=================
Sequence Diagrams
=================

**Start Bpod**

.. image:: /_images/sd_bpod_start.svg
   :scale: 100 %

**Send state machine**

.. image:: /_images/sd_bpod_send_sm.svg
   :scale: 100 %

**Run state machine**

.. image:: /_images/sd_bpod_run_sm.svg
   :scale: 100 %